import numpy
import scipy.stats as st
import seaborn as sns
import matplotlib.pyplot as plt

CANTIDAD_EXPERIMENTOS = 30  # Años
CANTIDAD_CORRIDAS = 250  # Días
FABRICACION_DIARIA = 130
COSTO_MANTENIMIENTO_UNIDAD = 70
INVENTARIO_INICIAL = 80


class Punto1:
    def __init__(self, unidades_minimas):
        self.unidades_minimas = unidades_minimas
        self.unidad_minima_actual = None
        self.costos_mantenimiento = []
        self.turnos_extra = []
        self.inventario = INVENTARIO_INICIAL

    def inicializar(self, unidad_minima_actual):
        self.unidad_minima_actual = unidad_minima_actual
        self.costos_mantenimiento = []
        self.turnos_extra = []
        self.inventario = INVENTARIO_INICIAL

    def ejecutar(self):
        for unidad_minima in self.unidades_minimas:
            print("Ejecución para "+str(unidad_minima)+" unidades mínimas")
            self.inicializar(unidad_minima)
            self.correr_simulacion(unidad_minima)

    def correr_simulacion(self, unidad_minima):
        for ejecucion in range(CANTIDAD_EXPERIMENTOS):
            turnos_extra = 0
            costos_mantenimientos_corrida = []

            for corrida in range(CANTIDAD_CORRIDAS):
                # Calcular demanda para el día
                demanda_diaria = numpy.random.normal(150, 25)
                # Ejecutamos la fabricación de un turno
                self.inventario += FABRICACION_DIARIA
                # Decrementamos el inventario en la demanda del día
                self.inventario -= int(demanda_diaria)
                # Verificamos si hay que agregar otro turno
                if self.inventario <= unidad_minima:
                    # Se agrega un turno más de fabricación
                    self.inventario += FABRICACION_DIARIA
                    turnos_extra += 1

                costos_mantenimientos_corrida.append(self.inventario * COSTO_MANTENIMIENTO_UNIDAD)

            self.costos_mantenimiento.append(sum(costos_mantenimientos_corrida)/len(costos_mantenimientos_corrida))
            self.turnos_extra.append(turnos_extra)

        self.generar_estadisticas()

    def generar_estadisticas(self):
        promedio_costo_mantenimiento = sum(self.costos_mantenimiento)/len(self.costos_mantenimiento)
        promedio_turnos_extra = sum(self.turnos_extra)/len(self.turnos_extra)
        print("El intervalo de confianza para el promedio anual de costo de mantenimiento (Con el 99% de confiabilidad) es de: ")
        conf_d, conf_d_izq, conf_d_der = self.mean_confidence_interval(self.costos_mantenimiento)
        print("+ Confianza: " + str(conf_d) + " (Izq: " + str(conf_d_izq) + " - Der: " + str(conf_d_der) + ")")
        print("Promedio Anual de Costo de Mantenimiento: " + str(promedio_costo_mantenimiento))
        self.generar_grafico(self.costos_mantenimiento, '', '', "Punto1-CostoMantenimiento-"+str(self.unidad_minima_actual)+"UnidadesMinimas.png")
        print("Promedio de turnos extra: " + str(promedio_turnos_extra))
        self.generar_grafico(self.turnos_extra, '', '', 'Punto1-TurnosExtra-'+str(self.unidad_minima_actual)+'UnidadesMinimas.png')

    @staticmethod
    def mean_confidence_interval(data, confidence=0.99):
        a = 1.0 * numpy.array(data)
        n = len(a)
        m, se = numpy.mean(a), st.sem(a)
        h = se * st.expon.ppf((1 + confidence) / 2., n - 1)
        return m, m - h, m + h

    @staticmethod
    def generar_grafico(datos, leyenda_x, leyenda_y, titulo_archivo):
        ax = sns.distplot(datos,
                          kde=True,
                          bins=100,
                          color='skyblue',
                          hist_kws={
                              "linewidth": 15,
                              'alpha': 1
                          }
                          )
        ax.set(xlabel=leyenda_x, ylabel=leyenda_y)
        plt.savefig('Graficos/' + titulo_archivo)
        plt.clf()
