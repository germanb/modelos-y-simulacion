from Enunciados.Enunciado1 import Enunciado1
from Enunciados.Enunciado2 import Enunciado2

EXPERIMENTOS = 30
EJECUCIONES = 100


if __name__ == '__main__':
    print("### Ejecución Enunciado 1:")
    enunciado1 = Enunciado1(EXPERIMENTOS, EJECUCIONES)
    enunciado1.ejecutar()
    print("### Ejecución Enunciado 2:")
    enunciado2 = Enunciado2(EXPERIMENTOS, EJECUCIONES)
    enunciado2.ejecutar()
