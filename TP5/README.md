# Para el lector:
A continuación se presentan los datos de la resolucion del Trabajo Práctico Nª5 de la cátedra Modelos y Simulacion (5to año de la carrera Licenciatura en Sistemas) de la Universidad Nacional de la Patagonia San Juan Bosco (Sede Trelew).

## Directorios y archivos de interes:
* El documento formal del práctico se encuentra [aquí](./TP5-MYS2020.pdf)
* Los gráficos se generan [aquí](./Graficos)
* Los enunciados estan resueltos [aquí](./Enunciados)
* El punto de entrada es [main.py](./main.py)

## Ejecución
Para la ejecución del presente proyecto basta con ejecutar en el directorio raíz:
> python main.py

**Nota:** La clase Enunciado (Abstracta) implementa todo el comportamiento de calculo y exportación de gráficos, mientras que las subclases (Concretas) sólo implementan la lógica de obtención de datos para cada corrida de cada experimento (Siendo llamados en cada ejecución mediante un template method).

# Resoluciones
A continuación se muestra la salida formateada de la ejecución del proyecto:

### Ejecución Enunciado 1:
El tiempo promedio de finalización del proyecto es: 
   
    29.97920004745145

El intervalo de confianza. Con el 99% de probabilidad (2,57) es de: 

    Confianza: 29.979200047451446 (Izq: 28.70941918137714 - Der: 31.248980913525752)

El porcentaje de criticidad que tienen los diferentes accesos es de: 
> Superior: 33.333333333333336%

> Medio: 0.0%

> Inferior: 66.66666666666667%

Los gráficos serán exportados a la carpeta de gráficos del proyecto (Indentificados por el problema y el tipo de grafico

### Ejecución Enunciado 2:
El tiempo promedio de finalización del proyecto es: 

    42.46739389702413

El intervalo de confianza. Con el 99% de probabilidad (2,57) es de: 

    Confianza: 42.46739389702413 (Izq: 39.98857944813514 - Der: 44.94620834591312)

El porcentaje de criticidad que tienen los diferentes accesos es de: 

> Superior: 0.0%

> Medio: 100.0%

> Inferior: 0.0%

Los gráficos serán exportados a la carpeta de gráficos del proyecto (Indentificados por el problema y el tipo de grafico