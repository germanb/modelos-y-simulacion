import numpy
import seaborn as sns
import scipy.stats as stats
import matplotlib.pyplot as plt
from abc import ABC, abstractmethod


class Enunciado(ABC):
    def __init__(self, titulo, num_experimentos = 0, num_ejecuciones = 0):
        self.titulo = titulo
        self.num_experimentos = num_experimentos
        self.num_ejecuciones = num_ejecuciones
        self.experimentos = []
        self.accesoSuperior = []
        self.accesoMedio = []
        self.accesoInferior = []

    @abstractmethod
    def inicializarValores(self):
        pass

    def ejecutar(self):
        if (self.num_experimentos == 0 or self.num_ejecuciones == 0):
            raise Exception("Ejecución mal configurada (Experimentos/Ejecuciones)")

        if (len(self.accesoSuperior) == 0 or len(self.accesoMedio) == 0 or len(self.accesoInferior) == 0):
            raise Exception("Ejecución no inicializada (Accesos sin datos)")

        for i in range(1, self.num_experimentos + 1):
            self.experimentos.append(self.ejecutarExperimento(i))
        self.procesarResultados()

    def ejecutarExperimento(self, num):
        resultado = {
            'id': num,
            'corridas': [],
            'resultados': []
        }

        # Ejecutar las corridas del experimento
        for i in range(1, self.num_ejecuciones + 1):
            self.inicializarValores()

            tiempo_superior = sum(map(lambda x: x['tiempo'], self.accesoSuperior))
            tiempo_medio = sum(map(lambda x: x['tiempo'], self.accesoMedio))
            tiempo_inferior = sum(map(lambda x: x['tiempo'], self.accesoInferior))

            resultado['corridas'].append({
                'id': i,
                'acceso_superior': tiempo_superior,
                'acceso_medio': tiempo_medio,
                'acceso_inferior': tiempo_inferior,
                'total': tiempo_superior + tiempo_medio + tiempo_inferior
            })

        # Procesar los resultados promedio de las corridas del experimento
        resultado['resultados'] = {
            'total': sum(map(lambda x: x['total'], resultado['corridas'])) / len(resultado['corridas']),
            'accesos': {
                'superior': sum(map(lambda x: x['acceso_superior'], resultado['corridas'])) / len(resultado['corridas']),
                'medio': sum(map(lambda x: x['acceso_medio'], resultado['corridas'])) / len(resultado['corridas']),
                'inferior': sum(map(lambda x: x['acceso_inferior'], resultado['corridas'])) / len(resultado['corridas'])
            }
        }

        return resultado

    def procesarResultados(self):
        totales = list(map(lambda x: x['resultados']['total'], self.experimentos))
        promedio_total = sum(totales) / len(self.experimentos)
        print("El tiempo promedio de finalización del proyecto es: "+str(promedio_total))

        print("El intervalo de confianza. Con el 99% de probabilidad (2,57) es de: ")
        conf_a, conf_a_izq, conf_a_der = self._mean_confidence_interval(totales)
        print("    Confianza: " + str(conf_a) + " (Izq: " + str(conf_a_izq) + " - Der: " + str(conf_a_der) + ")")

        print("El porcentaje de criticidad que tienen los diferentes accesos es de: ")
        print("> Superior: "+str(len(list(filter(lambda x: x['superior'] > x['medio'] and x['superior'] > x['inferior'], list(map(lambda x: x['resultados']['accesos'], self.experimentos)))))*100/len(self.experimentos))+"%")
        print("> Medio: "+str(len(list(filter(lambda x: x['medio'] > x['superior'] and x['medio'] > x['inferior'], list(map(lambda x: x['resultados']['accesos'], self.experimentos)))))*100/len(self.experimentos))+"%")
        print("> Inferior: "+str(len(list(filter(lambda x: x['inferior'] > x['medio'] and x['inferior'] > x['superior'], list(map(lambda x: x['resultados']['accesos'], self.experimentos)))))*100/len(self.experimentos))+"%")

        totales_corridas = []
        for experimento in self.experimentos:
            totales_corridas += list(map(lambda x: x['total'], experimento['corridas']))

        print("Los gráficos serán exportados a la carpeta de gráficos del proyecto (Indentificados por el problema y el tipo de grafico")
        self.exportarGrafico(totales_corridas, 'Total Corridas', '', self.titulo+'-Totales3000Corridas')
        self.exportarGrafico(totales, 'Total Experimentos', '', self.titulo+'-TotalesExperimentos')

    def _mean_confidence_interval(self, data, confidence=0.95):
        a = 1.0 * numpy.array(data)
        n = len(a)
        m, se = numpy.mean(a), stats.sem(a)
        h = se * stats.expon.ppf((1 + confidence) / 2., n-1)
        return m, m-h, m+h

    def exportarGrafico(self, datos, leyenda_x, leyenda_y, titulo_archivo):
        ax = sns.distplot(datos, 
            kde = True, 
            bins = 100, 
            color = 'skyblue', 
            hist_kws = {
                "linewidth": 15,
                'alpha':1
            }
        )
        ax.set(xlabel=leyenda_x, ylabel=leyenda_y)
        plt.savefig('Graficos/'+titulo_archivo)
        plt.clf()
