import numpy
import seaborn


from Enunciados.Enunciado import Enunciado


class Enunciado2(Enunciado):
    def __init__(self, experimentos, ejecuciones):
        super().__init__('Enunciado2', experimentos, ejecuciones)
        self.inicializarValores()

    def inicializarTareaInicial(self):
        return [
            {
                'id': 'A',
                'descripcion': 'Retirar alfombras',
                'tiempo': numpy.random.uniform(1, 5)
            }
        ]

    def inicializarAccesoSuperior(self):
        return [
            {
                'id': 'B',
                'descripcion': 'Aplicar detergente',
                'tiempo': numpy.random.uniform(1, 3)
            },
            {
                'id': 'C',
                'descripcion': 'Enjuagar alfombras',
                'tiempo': numpy.random.uniform(1, 3)
            }
        ]

    def inicializarAccesoMedio(self):
        return [
            {
                'id': 'D',
                'descripcion': 'Mojar vehículo',
                'tiempo': numpy.random.uniform(1, 6)
            },
            {
                'id': 'E',
                'descripcion': 'Aplicar detergente',
                'tiempo': numpy.random.uniform(6, 12)
            },
            {
                'id': 'F',
                'descripcion': 'Enjuagar vehículo',
                'tiempo': numpy.random.uniform(5, 10)
            }
        ]

    def inicializarAccesoInferior(self):
        return [
            {
                'id': 'G',
                'descripcion': 'Aspirar interiores',
                'tiempo': numpy.random.uniform(10, 15)
            }
        ]

    def inicializarValores(self):
        valor_inicial = self.inicializarTareaInicial()
        valores_acceso_superior = self.inicializarAccesoSuperior()
        valores_acceso_medio = self.inicializarAccesoMedio()
        self.accesoSuperior = valor_inicial + valores_acceso_superior
        self.accesoMedio = valor_inicial + valores_acceso_medio
        self.accesoInferior = self.inicializarAccesoInferior()
