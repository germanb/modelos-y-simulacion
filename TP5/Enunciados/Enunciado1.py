import numpy

from Enunciados.Enunciado import Enunciado


class Enunciado1(Enunciado):
    def __init__(self, num_experimentos, num_ejecuciones):
        super().__init__('Enunciado1', num_experimentos, num_ejecuciones)
        self.inicializarValores()

    def inicializarAccesoSuperior(self):
        return [
            {
                'id': 'A',
                'descripcion': 'Romper huevos',
                'tiempo': numpy.random.uniform(2, 4)
            },
            {
                'id': 'B',
                'descripcion': 'Revolver huevos',
                'tiempo': numpy.random.uniform(3, 6)
            },
            {
                'id': 'C',
                'descripcion': 'Cocinar huevos',
                'tiempo': numpy.random.uniform(2, 5)
            }
        ]

    def inicializarAccesoMedio(self):
        return [
            {
                'id': 'D',
                'descripcion': 'Cortar panes',
                'tiempo': numpy.random.uniform(3, 6)
            },
            {
                'id': 'E',
                'descripcion': 'Preparar tostadas',
                'tiempo': numpy.random.uniform(2, 5)
            }
        ]

    def inicializarAccesoInferior(self):
        return [
            {
                'id': 'F',
                'descripcion': 'Preparar bebidas calientes (té, café)',
                'tiempo': numpy.random.uniform(4, 8)
            },
            {
                'id': 'G',
                'descripcion': 'Preparar bebidas frías (jugos, yogur)',
                'tiempo': numpy.random.uniform(3, 7)
            }
        ]

    def inicializarValores(self):
        self.accesoSuperior = self.inicializarAccesoSuperior()
        self.accesoMedio = self.inicializarAccesoMedio()
        self.accesoInferior = self.inicializarAccesoInferior()
