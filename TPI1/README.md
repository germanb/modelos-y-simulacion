# Para el lector:
En el presente directorio se encuentra la resolucion del [Trabajo Práctico Integrador Nª1](./TPI1-MYS-2020.pdf) de la cátedra Modelos y Simulacion (5to año de la carrera Licenciatura en Sistemas) de la Universidad Nacional de la Patagonia San Juan Bosco (Sede Trelew).

## Simulación planteada
### Sistema
Cola bancaria de dos cajas.

### Consideraciones
* Hay solo dos cajas para atender clientes.
* Los clientes llegan siguiendo una distribución de Poisson.
* El tiempo de atención de un cliente sigue una distribución Exponencial.
* Los clientes tienen una cierta paciencia (Que sigue una distribución de Poisson) para esperar a que se libere alguna caja.

## Ejecución
Para la resolución de la simulación se plantea una situación normal de una cola bancaria para atender clientes. Dicha simulación se ejecuta corriendo el archivo [simulacion.py](./simulacion.py) de la siguiente manera:
> python simulacion.py

## Ejemplo de simulación
La siguiente salida nos da la consola para una simulación de 8 horas:

--- Llegaron 0 clientes [Hora: 1]

--- Llegaron 6 clientes [Hora: 2]

 0.0000 Cliente 00: Llegue!

 0.0000 Cliente 00: Esperé  0.000

 1.0000 Cliente 01: Llegue!

 1.0000 Cliente 01: Esperé  0.000

 2.0000 Cliente 02: Llegue!

 2.5772 Cliente 00: Ya fui atendido

 2.5772 Cliente 02: Esperé  0.577

 3.0000 Cliente 03: Llegue!

 4.0000 Cliente 04: Llegue!

 4.0958 Cliente 02: Ya fui atendido

 4.0958 Cliente 03: Esperé  1.096

 4.8633 Cliente 01: Ya fui atendido

 4.8633 Cliente 04: Esperé  0.863

 5.0000 Cliente 05: Llegue!

 6.0029 Cliente 04: Ya fui atendido

 6.0029 Cliente 05: Esperé  1.003

 7.0598 Cliente 05: Ya fui atendido

10.1618 Cliente 03: Ya fui atendido

--- Llegaron 5 clientes [Hora: 3]

10.1618 Cliente 00: Llegue!

10.1618 Cliente 00: Esperé  0.000

11.1618 Cliente 01: Llegue!

11.1618 Cliente 01: Esperé  0.000

12.1618 Cliente 02: Llegue!

13.1618 Cliente 03: Llegue!

13.9219 Cliente 01: Ya fui atendido

13.9219 Cliente 02: Esperé  1.760

14.1618 Cliente 04: Llegue!

14.1618 Cliente 03: Me cansé y me fui luego de esperar  1.000

14.5177 Cliente 02: Ya fui atendido

14.5177 Cliente 04: Esperé  0.356

17.3992 Cliente 00: Ya fui atendido

17.9688 Cliente 04: Ya fui atendido

--- Llegaron 3 clientes [Hora: 4]

17.9688 Cliente 00: Llegue!

17.9688 Cliente 00: Esperé  0.000

18.5215 Cliente 00: Ya fui atendido

18.9688 Cliente 01: Llegue!

18.9688 Cliente 01: Esperé  0.000

19.4303 Cliente 01: Ya fui atendido

19.9688 Cliente 02: Llegue!

19.9688 Cliente 02: Esperé  0.000

22.7308 Cliente 02: Ya fui atendido

--- Llegaron 3 clientes [Hora: 5]

23.9688 Cliente 00: Llegue!

23.9688 Cliente 00: Esperé  0.000

24.9688 Cliente 01: Llegue!

24.9688 Cliente 01: Esperé  0.000

25.9688 Cliente 02: Llegue!

27.9688 Cliente 02: Me cansé y me fui luego de esperar  2.000

30.0772 Cliente 01: Ya fui atendido

30.1790 Cliente 00: Ya fui atendido

--- Llegaron 0 clientes [Hora: 6]

--- Llegaron 1 clientes [Hora: 7]

30.1790 Cliente 00: Llegue!

30.1790 Cliente 00: Esperé  0.000

30.9370 Cliente 00: Ya fui atendido

--- Llegaron 3 clientes [Hora: 8]

33.1790 Cliente 00: Llegue!

33.1790 Cliente 00: Esperé  0.000

34.1790 Cliente 01: Llegue!

34.1790 Cliente 01: Esperé  0.000

35.1790 Cliente 02: Llegue!

35.1790 Cliente 02: Me cansé y me fui luego de esperar  0.000

35.8874 Cliente 01: Ya fui atendido

36.4148 Cliente 00: Ya fui atendido
