import time
import simpy
from scipy.stats import poisson
from scipy.stats import expon

HORAS = 8

def source(env, hora, cajas):
    """ Generar clientes segun la distribución de Poisson """
    cantidad_clientes = poisson.rvs(mu=2, size=1)[0]

    print("--- Llegaron " + str(cantidad_clientes) + " clientes [Hora: " + str(hora) + "]")

    for i in range(cantidad_clientes):
        env.process(customer(env, 'Cliente %02d' % i, cajas))
        yield env.timeout(1)


def customer(env, nombre, cajas):
    """ El cliente llega, se lo atiende y se va."""
    llegada = env.now
    print('%7.4f %s: Llegue!' % (llegada, nombre))

    with cajas.request() as req:
        paciencia = poisson.rvs(mu=2, size=1)
        # Esperar a que llegue el turno o bien perder la paciencia e irse
        resultado = yield req | env.timeout(paciencia)
        espera = env.now - llegada

        if req in resultado:
            print('%7.4f %s: Esperé %6.3f' % (env.now, nombre, espera))

            tiempo_atencion = expon.rvs(scale=4,loc=0,size=1)
            yield env.timeout(tiempo_atencion)
            print('%7.4f %s: Ya fui atendido' % (env.now, nombre))
        else:
            print('%7.4f %s: Me cansé y me fui luego de esperar %6.3f' % (env.now, nombre, espera))


env = simpy.Environment()
cajas = simpy.Resource(env, capacity=2)

# Simular las horas especificadas.
for hora in range(1, HORAS + 1):
    env.process(source(env, hora, cajas))
    env.run()
    time.sleep(30)  # Escala de tiempo dependiendo del límite superior dado por la exponencial
