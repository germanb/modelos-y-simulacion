from __future__ import annotations


class Evento:
    def __init__(self, obj, reloj, tipo):
        self._tipo = tipo
        self._obj = obj
        self._reloj = reloj

    @property
    def tipo(self):
        return self._tipo

    @property
    def objeto(self):
        return self._obj

    @property
    def reloj(self):
        return self._reloj

    @reloj.setter
    def reloj(self, value):
        self._reloj = value

    def aumentar_reloj(self, cantidad=1):
        self.reloj += cantidad
