import numpy as np
from .Evento import Evento


class Atendedor:
    def __init__(self, nombre, fx_tiempo_atencion):
        self._nombre = nombre
        self._entidad = None
        self._atenciones = 0
        self._tiempo_atencion = 0
        self._fx_tiempo_atencion = self._inicializar_fx(fx_tiempo_atencion)
        self._inicio_atencion = 0
        self._historico = []

    @property
    def nombre(self):
        return self._nombre

    @property
    def entidad(self):
        return self._entidad

    @entidad.setter
    def entidad(self, entidad):
        self._entidad = entidad

    @property
    def atenciones(self):
        return self._atenciones

    @property
    def tiempo_atencion(self):
        return self._tiempo_atencion

    @property
    def fx_tiempo_atencion(self):
        return self._fx_tiempo_atencion

    @property
    def inicio_atencion(self):
        return self._inicio_atencion

    @inicio_atencion.setter
    def inicio_atencion(self, inicio_atencion):
        self._inicio_atencion = inicio_atencion

    @property
    def historico(self):
        return self._historico

    def _inicializar_fx(self, distribucion):
        if distribucion['tipo'] == 'normal':
            return lambda x: np.random.normal(distribucion["params"]["media"], distribucion["params"]["desvio"])
        elif distribucion['tipo'] == 'exponencial':
            return lambda x: np.random.exponential(distribucion['params']['lambda'])
        else:
            raise Exception("Distribución no soportada")

    def _incrementar_atenciones(self):
        self._atenciones += 1

    def _calcular_tiempo_atencion(self):
        self._tiempo_atencion = self._fx_tiempo_atencion('x')

    def ocupar(self, entidad, reloj_global):
        self._incrementar_atenciones()
        self._calcular_tiempo_atencion()
        self.entidad = entidad
        self.inicio_atencion = reloj_global
        return Evento(self, self.tiempo_atencion + reloj_global, 'SALIDA')

    def desocupar(self, reloj_global):
        self.entidad.registrar_salida(reloj_global)
        self._entidad = None
        self.agregar_historico(reloj_global)

    def ocupado(self):
        return self.entidad is not None

    def agregar_historico(self, reloj_global):
        self.historico.append(reloj_global - self.inicio_atencion)
        self.inicio_atencion = 0
