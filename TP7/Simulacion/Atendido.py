class Atendido:
    def __init__(self, nombre, reloj_llegada):
        self._nombre = nombre
        self._tiempo_espera = 0
        self._reloj_llegada = 0
        self._reloj_salida = 0
        self.registrar_llegada(reloj_llegada)

    @property
    def nombre(self):
        return self._nombre

    @property
    def tiempo_espera(self):
        return self._tiempo_espera

    @property
    def reloj_llegada(self):
        return self._reloj_llegada

    @property
    def reloj_salida(self):
        return self._reloj_salida

    def aumentar_espera(self, espera=1):
        self._tiempo_espera += espera

    def registrar_llegada(self, reloj_llegada):
        self._reloj_llegada = reloj_llegada

    def registrar_salida(self, reloj_salida):
        self._reloj_salida = reloj_salida
