from __future__ import annotations
import numpy as np
import random
from abc import ABC, abstractmethod
from Config.Configuracion import Configuracion
from Simulacion.Atendedor import Atendedor
from Simulacion.Evento import Evento
from Simulacion.Atendido import Atendido
from Estadisticas.EstadisticasPunto1 import EstadisticasPunto1
from Estadisticas.EstadisticasPunto2 import EstadisticasPunto2


class Simulacion(ABC):
    def __init__(self, configfile, estadisticas_class):
        self._configuracion = Configuracion.cargar_configuracion(configfile)
        self._estadisticas = self.get_estadisticas_object(estadisticas_class)
        self._atendedores = []
        self._cola_eventos = []
        self.reloj_global = 0
        self.experimentos = []
        self.inicializar_simulacion()

    @property
    def atendedores(self):
        return self._atendedores

    @property
    def cola_eventos(self):
        return self._cola_eventos

    @property
    def configuracion(self):
        return self._configuracion

    @property
    def estadisticas(self):
        return self._estadisticas

    @estadisticas.setter
    def estadisticas(self, estadisticas):
        self._estadisticas = estadisticas

    def reiniciar_reloj_global(self):
        self.reloj_global = 0

    def reiniciar_atendedores(self):
        self._atendedores = []

    def reiniciar_cola_eventos(self):
        self._cola_eventos = []

    @abstractmethod
    def inicializar_simulacion(self):
        pass

    def get_estadisticas_object(self, clase):
        if clase == 'Punto1':
            return EstadisticasPunto1(clase)
        elif clase == 'Punto2':
            return EstadisticasPunto2(clase)
        else:
            raise Exception("Clase no reconocida para la inicializacion de las estadisticas")

    def inicializar_estadisticas(self):
        for atendedor in self.atendedores:
            self.estadisticas.cantidad_atendidos_por_atendedor[atendedor.nombre] = 0

    def inicializar_atendidos(self):
        acumulador = 0

        while True:
            tiempo_llegada = np.random.exponential(self.configuracion['atendidos']['lambda_llegada'])

            if tiempo_llegada + acumulador <= self.configuracion['tiempo_simulacion']:
                acumulador += tiempo_llegada

                self.agregar_a_la_cola_de_eventos(Evento(
                    Atendido(self.configuracion['atendidos']['nombre'], acumulador),
                    acumulador,
                    'LLEGADA')
                )
            else:
                break

    def inicializar_atendedores(self):
        for atendedor in self.configuracion['atendedores']:
            self.atendedores.append(Atendedor(atendedor["nombre"], atendedor["distribucion"]))

    def atendedores_estan_ocupados(self):
        for atendedor in self.atendedores:
            if not atendedor.ocupado():
                return False
        return True

    def obtener_atendedor_desocupado(self):
        return random.choice(list(filter(lambda x: not x.ocupado(), self.atendedores)))

    def reordenar_cola_de_eventos(self):
        self._cola_eventos = sorted(self.cola_eventos, key=lambda k: k.reloj)

    def agregar_a_la_cola_de_eventos(self, evento):
        self.cola_eventos.append(evento)
        self.reordenar_cola_de_eventos()

    def procesar_evento(self, evento):
        if evento.tipo == 'LLEGADA':
            if not self.atendedores_estan_ocupados():
                atendedor = self.obtener_atendedor_desocupado()
                self.agregar_a_la_cola_de_eventos(atendedor.ocupar(evento.objeto, self.reloj_global))
            else:
                evento.objeto.aumentar_espera()
                evento.aumentar_reloj()
                self.reordenar_cola_de_eventos()
        else:
            evento.objeto.desocupar(self.reloj_global)

    def obtener_siguiente_evento(self):
        return next((evento for evento in self.cola_eventos if evento.reloj > self.reloj_global), None)

    @abstractmethod
    def ejecutar(self):
        pass
