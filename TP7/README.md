# Para el lector:
A continuación se presentan los datos de la resolucion del Trabajo Práctico Nª7 de la cátedra Modelos y Simulacion (5to año de la carrera Licenciatura en Sistemas) de la Universidad Nacional de la Patagonia San Juan Bosco (Sede Trelew).

## Directorios y archivos de interes:
* El documento formal del práctico se encuentra [aquí](./TP7-MYS2020.pdf)
* Los gráficos se generan [aquí](./Graficos)
* La configuración de cada enunciado esta dada en archivos JSON [aquí](./Config)
* Los enunciados estan resueltos [aquí](./Punto1.py) y [aquí](./Punto2.py)
* El punto de entrada es [main.py](./main.py)

## Ejecución
Para la ejecución del presente proyecto basta con ejecutar en el directorio raíz:
> python main.py

**Nota:** Al ser muy similares los dos enunciados, se planteó la resolución de los mismos mediante una sola clase Simulación e implementaciones genéricas para las entidades más importantes. Luego, cada ejecución se configura con los archivos JSON de configuración presentes en el directorio de configuraciones (De donde se saca todo lo necesario para cada ejecución).
