from __future__ import annotations
import os
import json


class Configuracion:
    @staticmethod
    def cargar_configuracion(archivo):
        path = os.path.join(os.path.dirname(__file__), archivo)
        if os.path.exists(path):
            with open(path, 'r') as stream:
                return json.load(stream)
        else:
            raise Exception("El archivo de configuracion especificado no existe")
