from abc import ABC, abstractmethod
from Simulacion.Utiles import mean_confidence_interval, exportar_grafico


class Estadisticas(ABC):
    def __init__(self, clase):
        self.nombre_clase = clase
        self._experimentos = []
        self.cantidad_total_atenciones = 0
        self.cantidad_atendidos_por_atendedor = {}
        self.promedios_esperas_por_corrida = []
        self.promedio_esperas_por_experimentos = []

    @property
    def experimentos(self):
        return self._experimentos

    def generar(self):
        self._calcular_datos()
        self._calcular_intervalo_confianza()
        self._calcular_promedio_esperas()
        self._porcentaje_de_ocupacion()
        self._generarHistograma()

    @abstractmethod
    def _calcular_datos(self):
        pass

    def _calcular_intervalo_confianza(self):
        print("# El intervalo de confianza para los promedios de esperas por experimento. Con el 99% de probabilidad es de: ")
        conf_d, conf_d_izq, conf_d_der = mean_confidence_interval(self.promedio_esperas_por_experimentos)
        print("+ Confianza: " + str(conf_d) + " (Izq: " + str(conf_d_izq) + " - Der: " + str(conf_d_der) + ")")

    def _calcular_promedio_esperas(self):
        print("+ El promedio de esperas de cada experimento fue: " + str(self.promedio_esperas_por_experimentos))
        print("+ El promedio general de las esperas es: " + str(
            sum(self.promedio_esperas_por_experimentos) / len(self.promedio_esperas_por_experimentos)))

    def _porcentaje_de_ocupacion(self):
        print("El porcentaje de ocupación de los atendedores (Respecto al total: "+str(self.cantidad_total_atenciones)+") es de: ")
        for atendedor, cantidad in self.cantidad_atendidos_por_atendedor.items():
            print("- "+atendedor+": "+str(cantidad)+" ("+str(cantidad / self.cantidad_total_atenciones)+"%)")

    def _generarHistograma(self):
        exportar_grafico(
            self.promedio_esperas_por_experimentos,
            '',
            '',
            self.nombre_clase+'(' + str(len(self.cantidad_atendidos_por_atendedor)) + '-Surt).png'
        )
        print("+ El gráfico del tiempo promedio de espera de los experimentos fue exportado en la carpeta de gráficos")
