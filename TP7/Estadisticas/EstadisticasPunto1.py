from Estadisticas.Estadisticas import Estadisticas


class EstadisticasPunto1(Estadisticas):
    def _calcular_datos(self):

        self.promedio_esperas_por_experimentos = []

        for experimento in self.experimentos:
            self.promedios_esperas_por_corrida = []

            for corrida in experimento:
                esperas = [evento.objeto.tiempo_espera for evento in corrida if evento.tipo == 'LLEGADA']
                self.promedios_esperas_por_corrida.append(sum(esperas) / len(esperas))

                for evento in corrida:
                    if evento.tipo == 'SALIDA':
                        self.cantidad_atendidos_por_atendedor[evento.objeto.nombre] += evento.objeto.atenciones
                        self.cantidad_total_atenciones += evento.objeto.atenciones

            self.promedio_esperas_por_experimentos.append(
                sum(self.promedios_esperas_por_corrida) / len(self.promedios_esperas_por_corrida))