from Estadisticas.Estadisticas import Estadisticas


class EstadisticasPunto2(Estadisticas):
    def _calcular_datos(self):
        self.promedio_esperas_por_experimentos = []

        for experimento in self.experimentos:
            esperas = [evento.objeto.tiempo_espera for evento in experimento if evento.tipo == 'LLEGADA']
            self.promedio_esperas_por_experimentos.append(sum(esperas) / len(esperas))

            for evento in experimento:
                if evento.tipo == 'SALIDA':
                    self.cantidad_atendidos_por_atendedor[evento.objeto.nombre] += evento.objeto.atenciones
                    self.cantidad_total_atenciones += evento.objeto.atenciones
