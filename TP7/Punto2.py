from Simulacion.Simulacion import Simulacion


class Punto2(Simulacion):
    def __init__(self, configfile):
        Simulacion.__init__(self, configfile, self.__class__.__name__)

    def inicializar_simulacion(self):
        self.reiniciar_reloj_global()
        self.reiniciar_cola_eventos()
        self.reiniciar_atendedores()
        self.inicializar_atendedores()
        self.inicializar_atendidos()
        self.inicializar_estadisticas()

    def ejecutar(self):
        for _ in range(self.configuracion['experimentos']):  # 365 DIAS
            self.inicializar_simulacion()

            while True:
                evento = self.obtener_siguiente_evento()

                if evento is not None:
                    self.reloj_global = evento.reloj
                    self.procesar_evento(evento)
                else:
                    break

            self.estadisticas.experimentos.append(self.cola_eventos)
        self.estadisticas.generar()
