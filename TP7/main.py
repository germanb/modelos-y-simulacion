from Punto1 import Punto1
from Punto2 import Punto2

PUNTO1_1 = 'Punto1-4surtidores.json'
PUNTO1_2 = 'Punto1-5surtidores.json'
PUNTO2_1 = 'Punto2-2cajas.json'
PUNTO2_2 = 'Punto2-3cajas.json'


if __name__ == '__main__':
    simulacion_punto_1 = Punto1(PUNTO1_1)
    simulacion_punto_1.ejecutar()
    simulacion_punto_1 = Punto1(PUNTO1_2)
    simulacion_punto_1.ejecutar()
    simulacion_punto_2 = Punto2(PUNTO2_1)
    simulacion_punto_2.ejecutar()
    simulacion_punto_2 = Punto2(PUNTO2_2)
    simulacion_punto_2.ejecutar()
