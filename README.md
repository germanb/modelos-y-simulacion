# Universidad Nacional de la Patagonia San Juan Bosco
## Asignatura: Modelos y Simulacion (5to Año)
## Sede Trelew

En el presente repositorio se pueden encontrar las resoluciones a todos los trabajos prácticos de la materia Modelos y Simulacion.

### Directorios
* [Trabajo Práctico Nª1](./TP1)
* [Trabajo Práctico Nª2](./TP2)
* [Trabajo Práctico Nª3](./TP3)
* [Trabajo Práctico Integrador Nª1](./TPI1)
