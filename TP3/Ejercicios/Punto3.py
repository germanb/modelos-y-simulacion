import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt

from scipy.stats import uniform
from scipy.stats import norm
from scipy.stats import poisson
from scipy.stats import expon


class Punto3:
    _imprimir_muestras = False
    _muestra = None
    _inciso_a = None
    _inciso_c = None
    _inciso_d = None

    def __init__(self, muestra, imprimir = False):
        self._muestra = muestra
        self._imprimir_muestras = imprimir

    def get_datos(self):
        return {
            'muestra': self._muestra,
            'inciso_a': self._inciso_a,
            'inciso_c': self._inciso_c,
            'inciso_d': self._inciso_d
        }

    def resolucion(self):
        print("""## Enunciado:
A partir de la muestra obtenida de 1000 valores en el punto 2.a, realice:
    a. Una transformación para que la variable aleatoria tenga una distribución de probabilidad Exponencial con parámetro λ = 8.
    b. Un gráfico adecuado para variable aleatoria.
    c. Genere otros 100 valores aleatorios siguiendo una distribución Exponencial con parámetro λ = 2.
    d. Genere 1000 valores aleatorios con las mismas características que en el inciso c.
    e. Grafique los incisos c y d.
    f. Compare resultados para 100 y 1000 valores describiendo similitudes o diferencias encontradas.
        """)
        print("## Resolucion:")
        self.inciso_a()
        self.inciso_b()
        self.inciso_c()
        self.inciso_d()
        self.inciso_e()
        self.inciso_f()
        print("")

    def inciso_a(self):
        print("### Inciso a: Una transformación para que la variable aleatoria tenga una distribución de probabilidad Exponencial con parámetro λ = 8.")

        muestra_expon = []
        for i in self._muestra:
            muestra_expon.append(-8**-1 * np.log(i))

        if self._imprimir_muestras:
            print(">>> La muestra uniforme tras su transformacion queda:")
            print(muestra_expon)

        self._inciso_a = muestra_expon

    def inciso_b(self):
        print("### Inciso b: Un gráfico adecuado para variable aleatoria.")

        transformado = self.get_datos()['inciso_a']
        ax = sns.distplot(transformado, 
            bins = 100, 
            kde = True, 
            color = 'skyblue', 
            hist_kws = {
                "linewidth": 15,
                'alpha':1
            }
        )
        ax.set(xlabel='Distribucion Uniforme Exponenciada', ylabel='Frecuencia')
        plt.savefig('Graficos/Punto3-inciso_b.png')

        print(">>> El gráfico correspondiente fue generado en el directorio de los Graficos, identificado por el ejercicio e inciso.")

    def inciso_c(self):
        print("### Inciso c: Genere otros 100 valores aleatorios siguiendo una distribución Exponencial con parámetro λ = 2.")

        data_expon = expon.rvs(scale=0.5,loc=0,size=100)

        if self._imprimir_muestras:
            print(">>> La muestra generada es:")
            print(data_expon)

        media = np.mean(data_expon)
        print(">>> Media: " + str(media))
        desvio = np.nanstd(data_expon)
        print(">>> Desvio Estandar:" + str(desvio))
        variancia = np.var(data_expon)
        print(">>> Variancia: " + str(variancia)) # Variancia = 1/2^2 

        self._inciso_c = data_expon

    def inciso_d(self):
        print("### Inciso d: Genere 1000 valores aleatorios con las mismas características que en el inciso c.")

        data_expon = expon.rvs(scale=0.5,loc=0,size=1000)
        if self._imprimir_muestras:
            print(">>> Los datos generados para la variable aletoria son: ")
            print(data_expon)

        media = np.mean(data_expon)
        print(">>> Media: " +str(media))
        desvio = np.nanstd(data_expon)
        print(">>> Desvio Estandar: " +str(desvio))
        variancia = np.var(data_expon)
        print(">>> Variancia: " +str(variancia)) # Variancia = 1/2^2 

        self._inciso_d = data_expon

    def inciso_e(self):
        print("### Inciso e: Grafique los incisos c y d.")

        ax = sns.distplot(self.get_datos()['inciso_c'], 
            kde = True, 
            bins = 100, 
            color = 'skyblue', 
            hist_kws = {
                "linewidth": 15,
                'alpha':1
            }
        )
        ax.set(xlabel='Inciso c', ylabel='Frecuencia')
        plt.savefig('Graficos/Punto3-inciso_e1.png')

        ax = sns.distplot(self.get_datos()['inciso_d'], 
            kde = True, 
            bins = 100, 
            color = 'skyblue', 
            hist_kws = {
                "linewidth": 15,
                'alpha':1
            }
        )
        ax.set(xlabel='Inciso d', ylabel='Frecuencia')
        plt.savefig('Graficos/Punto3-inciso_e2.png')

        print(">>> Los gráficos correspondientes fueron generados en el directorio de los Graficos, identificados por el ejercicio e inciso correspondientes.")

    def inciso_f(self):
        print("### Inciso f: Compare resultados para 100 y 1000 valores describiendo similitudes o diferencias encontradas.")
        print(">>> Contestado en el documento del trabajo")
