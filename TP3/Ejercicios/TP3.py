from Ejercicios.Punto1 import Punto1
from Ejercicios.Punto2 import Punto2
from Ejercicios.Punto3 import Punto3
from Ejercicios.Punto4 import Punto4


class Tp3:
    _imprimir = False

    def __init__(self, imprimir = False):
        self._imprimir = imprimir

    def resolucion(self):
        print("""
#############################################################################
# A continuación se presentan los datos de la resolucion de cada inciso del #
# Trabajo Práctico Nª3 de la cátedra Modelos y Simulacion (5to año de la    #
# carrera Licenciatura en Sistemas) de la Universidad Nacional de la        #
# Patagonia San Juan Bosco (Sede Trelew).                                   #
#                                                                           #
# Año: 2020                                                                 #
#############################################################################
        """)

        print("# Ejercicio 1")
        punto1 = Punto1(imprimir = self._imprimir)
        punto1.resolucion()
        print("# Ejercicio 2")
        punto2 = Punto2(imprimir = self._imprimir)
        punto2.resolucion()
        print("# Ejercicio 3")
        punto3 = Punto3(muestra=punto2.get_datos()['inciso_a']['muestra'], imprimir = self._imprimir)
        punto3.resolucion()
        print("# Ejercicio 4")
        punto4 = Punto4(datos_punto_3 = punto3.get_datos(), imprimir = self._imprimir)
        punto4.resolucion()
