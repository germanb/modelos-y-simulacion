sink("output.txt")
print("Generando numeros aleatorios para una distribucion normal de probabilidad...")
normal <- rnorm(100)
print("Los numeros generados fueron:")
print(normal)

print("La media es: ")
media <- mean(normal)
print(media)

print("El desvio estandar es: ")
desv = sd(normal)
print(desv)

print("La variancia es: ")
print(var(normal))


eje_x <- seq(-10, 10, by = .1)
eje_y <- dnorm(eje_x, media, desv)
png(file = "punto1.png")
plot(eje_x, eje_y)
dev.off()

print("Se guardo el grafico para este conjunto de datos en la carpeta actual")
sink()
