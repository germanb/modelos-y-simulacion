import matplotlib.pyplot as plt
from IPython.core.display import Image
import seaborn as sns
import numpy as np
from scipy.stats import expon

class Punto1:
    _imprimir_muestra = False
    _muestra = None
    _media = None
    _desvio = None
    _variancia = None

    def __init__(self, imprimir = False):
        self._imprimir_muestra = imprimir

    def get_datos(self):
        return {
            'muestra': self._muestra,
            'media': self._media,
            'desvio': self._desvio,
            'variancia': self._variancia
        }

    def resolucion(self):
        print("""## Enunciado:
Investigue y documente al menos 3 lenguajes de programación que ofrezcan la posibilidad de generar números aleatorios que sigan una distribución probabilística ya sea de modo nativo o bien a través del uso de librerías externas. 
Genere un conjunto de 100 datos numéricos para una variable imaginaria y con ayuda de los lenguajes:
    a. Calcule la media.
    b. Calcule el desvío estándar.
    c. Calcule la varianza. 
    d. ¿Cómo se crean gráficos para interpretar los valores generados?
        """)
        print("## Resolucion:")
        print("Para la resolucion de este ejercicio se eligieron los lenguajes Python, R e IDL")
        print("El desarrollo de python esta dado en el presente código, el de R se puede encontrar en este mismo repositorio (Con su correspondiente Dockerfile) y el de IDL se deja como investigacion unicamente")

        self.obtencion_muestra()
        self.inciso_a()
        self.inciso_b()
        self.inciso_c()
        self.inciso_d()
        print("")

    def obtencion_muestra(self):
        data_expon = expon.rvs(scale=1,loc=0,size=100)

        if self._imprimir_muestra:
            print(">>> La muestra obtenida (Segun la distribuciòn exponencial) es la siguiente: ")
            print(data_expon)

        self._muestra = data_expon

    def inciso_a(self):
        self._media = np.mean(self._muestra)
        print("### Inciso a: ")
        print(">>> La media nos da: "+str(self._media))

        # Generamos el gráfico.

    def inciso_b(self):
        self._desvio = np.nanstd(self._muestra)
        print("### Inciso b: ")
        print(">>> El desvío estándar nos da: "+str(self._desvio))

    def inciso_c(self):
        self._variancia = np.var(self._muestra)
        print("### Inciso c: ")
        print(">>> La variancia nos da: "+str(self._variancia))

    def inciso_d(self):
        print("### Inciso d: ") 
        print(">>> Se utilizó la librería matplotlib en conjunto con la librerìa seaborn para la generación del gráfico (El mismo puede ser encontrado en El directorio de los ejercicios, identificado por el nombre del ejercicio).")
        sns.set(color_codes=True)
        sns.set(rc={'figure.figsize':(5,5)})
        ax = sns.distplot(self._muestra,
                        kde=True,
                        bins=100,
                        color='skyblue',
                        hist_kws={"linewidth": 15,'alpha':1})
        ax.set(xlabel='Exponencial', ylabel='Frecuencia')

        # Guardamos el gráfico.
        plt.savefig('Graficos/Punto1.png')
        print(">>> El gráfico correspondiente fue generado en el directorio de los Graficos, identificado por el ejercicio e inciso.")
