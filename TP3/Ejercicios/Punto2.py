import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt

from scipy.stats import uniform
from scipy.stats import norm
from scipy.stats import poisson
from scipy.stats import expon


class Punto2:
    _imprimir_muestras = False
    _inciso_a = None
    _inciso_b = None
    _inciso_c = None
    _inciso_d = None

    def __init__(self, imprimir = False):
        self._imprimir_muestras = imprimir

    def get_datos(self):
        return {
            'inciso_a': self._inciso_a,
            'inciso_b': self._inciso_b,
            'inciso_c': self._inciso_c,
            'inciso_d': self._inciso_d
        }

    def resolucion(self):
        print("""## Enunciado:
Teniendo en cuenta el material teórico y los lenguajes investigados en el punto 1, elija uno de ellos y genere 1000 valores de números aleatorios:
    a. Uniforme, con parámetros: Min: 0, Max: 1. 
    b. Normal, con parámetros: Media: 0, Desvío: 1.
    c. Poisson, con parámetro: λ = 5. 
    d. Exponencial, con parámetro:  = ¼. 
        """)
        print("## Resolucion:")
        self.inciso_a()
        self.inciso_b()
        self.inciso_c()
        self.inciso_d()
        print("")

    def inciso_a(self):
        print("### Inciso a (Uniforme):")
        n = 1000
        start = 0
        width = 1
        data_uniform = uniform.rvs(size=n, loc = start, scale=width)

        if self._imprimir_muestras:
            print(">>> Los datos generados para la variable aletoria son: ")
            print(data_uniform)

        media = np.mean(data_uniform)
        print(">>> Media: " + str(media))
        desvio = np.nanstd(data_uniform)
        print(">>> Desvío estándar: " + str(desvio))
        variancia = np.var(data_uniform)
        print(">>> Variancia: " + str(variancia))

        ax = sns.distplot(data_uniform, bins=100, kde=True, color='skyblue', hist_kws={"linewidth": 15,'alpha':1})
        ax.set(xlabel='Distribucion Uniforme', ylabel='Frecuenccia')
        plt.savefig('Graficos/Punto2-inciso_a.png')
        print(">>> El gráfico correspondiente fue generado en el directorio de los Graficos, identificado por el ejercicio e inciso.")
        self._inciso_a = {
            'muestra': data_uniform,
            'media': media,
            'desvio': desvio,
            'variancia': variancia
        }

    def inciso_b(self):
        print("### Inciso b (Normal):")

        data_norm = norm.rvs(scale=1,loc=0,size=1000)

        if self._imprimir_muestras:
            print(">>> Los datos generados para la variable aletoria son: ")
            print(data_norm)

        media = np.mean(data_norm)
        print(">>> Media: " + str(media))
        desvio = np.nanstd(data_norm)
        print(">>> Desvío estándar: " + str(desvio))
        variancia = np.var(data_norm)
        print(">>> Variancia: " + str(variancia))

        ax = sns.distplot(data_norm, kde=True, bins=100, color='skyblue', hist_kws={"linewidth": 15,'alpha':1})
        ax.set(xlabel='Distribucion Normal', ylabel='Frecuencia')
        plt.savefig('Graficos/Punto2-inciso_b.png')

        print(">>> El gráfico correspondiente fue generado en el directorio de los Graficos, identificado por el ejercicio e inciso.")
        self._inciso_b = {
            'muestra': data_norm,
            'media': media,
            'desvio': desvio,
            'variancia': variancia
        }

    def inciso_c(self):
        print("### Inciso c (Poisson):")

        data_poisson = poisson.rvs(mu=5, size=1000)

        if self._imprimir_muestras:
            print(">>> Los datos generados para la variable aletoria son: ")
            print(data_poisson)

        media = np.mean(data_poisson)
        print(">>> Media: " + str(media))
        desvio = np.nanstd(data_poisson)
        print(">>> Desvío estándar: " + str(desvio))
        variancia = np.var(data_poisson)
        print(">>> Variancia: " + str(variancia))

        ax = sns.distplot(data_poisson, kde=True, bins=100, color='skyblue', hist_kws={"linewidth": 15,'alpha':1})
        ax.set(xlabel='Distribucion Poisson', ylabel='Frecuencia')
        plt.savefig('Graficos/Punto2-inciso_c.png')

        print(">>> El gráfico correspondiente fue generado en el directorio de los Graficos, identificado por el ejercicio e inciso.")
        self._inciso_c = {
            'muestra': data_poisson,
            'media': media,
            'desvio': desvio,
            'variancia': variancia
        }

    def inciso_d(self):
        print("### Inciso d (Exponencial):")

        data_expon = expon.rvs(scale=4,loc=0,size=1000)
        if self._imprimir_muestras:
            print(">>> Los datos generados para la variable aletoria son: ")
            print(data_expon)

        media = np.mean(data_expon)
        print(">>> Media: " + str(media))
        desvio = np.nanstd(data_expon)
        print(">>> Desvio Estandar:" + str(desvio))
        variancia = np.var(data_expon)
        print(">>> Varianza: " + str(variancia))

        ax = sns.distplot(data_expon, kde=True, bins=100, color='skyblue', hist_kws={"linewidth": 15,'alpha':1})
        ax.set(xlabel='Distribucion Exponencial', ylabel='Frecuencia')
        plt.savefig('Graficos/Punto2-inciso_d.png')

        print(">>> El gráfico correspondiente fue generado en el directorio de los Graficos, identificado por el ejercicio e inciso.")
        self._inciso_d = {
            'muestra': data_expon,
            'media': media,
            'desvio': desvio,
            'variancia': variancia
        }
