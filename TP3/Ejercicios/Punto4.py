import matplotlib.pyplot as plt
from IPython.core.display import Image
import seaborn as sns
import numpy as np
import scipy.stats as stats

class Punto4:
    _imprimir_muestra = False
    _datos_punto3 = None
    _inciso_a = None
    _inciso_b = None
    _inciso_c = None
    
    def __init__(self, datos_punto_3, imprimir = False):
        self._imprimir_muestra = imprimir
        self._datos_punto3 = datos_punto_3

    def get_datos(self):
        return {
            'inciso_a': self._inciso_a,
            'inciso_b': self._inciso_b,
            'inciso_c': self._inciso_c
        }

    def resolucion(self):
        print("""## Enunciado:
De acuerdo a los resultados obtenidos en el ejercicio anterior. Especifique:
    a. ¿Cuál fue la media muestral de los datos generados?
    b. ¿Cuál fue la varianza muestral de los datos generados?
    c. Realice un intervalo de confianza, con  3 desvíos estándar.
        """)
        print("## Resolucion:")
 
        self.inciso_a()
        self.inciso_b()
        self.inciso_c()
        print("")

    def inciso_a(self):
        print("### Inciso a:")
        media_a = np.mean(self._datos_punto3['inciso_a'])
        print(">>> La media de los datos muestrales obtenidos en el inciso a fue de: " + str(media_a))
        media_c = np.mean(self._datos_punto3['inciso_c'])
        print(">>> La media de los datos muestrales obtenidos en el inciso c fue de: " + str(media_c))
        media_d = np.mean(self._datos_punto3['inciso_d'])
        print(">>> La media de los datos muestrales obtenidos en el inciso d fue de: " + str(media_d))

        self._inciso_a = {
            'media_a': media_a,
            'media_c': media_c,
            'media_d': media_d
        }

    def inciso_b(self):
        print("### Inciso b:")
        variancia_a = np.var(self._datos_punto3['inciso_a'])
        print(">>> La variancia de los datos muestrales obtenidos en el inciso a fue de: " + str(variancia_a))
        variancia_c = np.var(self._datos_punto3['inciso_c'])
        print(">>> La variancia de los datos muestrales obtenidos en el inciso c fue de: " + str(variancia_c))
        variancia_d = np.var(self._datos_punto3['inciso_d'])
        print(">>> La variancia de los datos muestrales obtenidos en el inciso d fue de: " + str(variancia_d))

        self._inciso_b = {
            'variancia_a': variancia_a,
            'variancia_c': variancia_c,
            'variancia_d': variancia_d
        }

    def inciso_c(self):
        print("### Inciso c:")
        print(">>> Los intervalos de confianza correspondientes al inciso a estan dados por: ")
        conf_a, conf_a_izq, conf_a_der = self._mean_confidence_interval(self._datos_punto3['inciso_a'])
        print("    Confianza: " + str(conf_a) + "(Izq: " + str(conf_a_izq) + " - Der: " + str(conf_a_der) + ")")
        print(">>> Los intervalos de confianza correspondientes al inciso c estan dados por: ")
        conf_c, conf_c_izq, conf_c_der = self._mean_confidence_interval(self._datos_punto3['inciso_c'])
        print("    Confianza: " + str(conf_c) + "(Izq: " + str(conf_c_izq) + " - Der: " + str(conf_c_der) + ")")
        print(">>> Los intervalos de confianza correspondientes al inciso d estan dados por: ")
        conf_d, conf_d_izq, conf_d_der = self._mean_confidence_interval(self._datos_punto3['inciso_d'])
        print("    Confianza: " + str(conf_d) + "(Izq: " + str(conf_d_izq) + " - Der: " + str(conf_d_der) + ")")
        
        self._inciso_c = {
            'inciso_a': {
                'confianza': conf_a,
                'limite_izquierdo': conf_a_izq,
                'limite_derecho': conf_a_der
            },
            'inciso_c': {
                'confianza': conf_c,
                'limite_izquierdo': conf_c_izq, 
                'limite_derecho': conf_c_der
            },
            'inciso_d': {
                'confianza': conf_d,
                'limite_izquierdo': conf_d_izq,
                'limite_derecho': conf_d_der
            }
        }

    def _mean_confidence_interval(self, data, confidence=0.95):
        a = 1.0 * np.array(data)
        n = len(a)
        m, se = np.mean(a), stats.sem(a)
        h = se * stats.expon.ppf((1 + confidence) / 2., n-1)
        return m, m-h, m+h
