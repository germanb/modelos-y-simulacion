# Para el lector:
A continuación se presentan los datos de la resolucion del Trabajo Práctico Nª3 de la cátedra Modelos y Simulacion (5to año de la carrera Licenciatura en Sistemas) de la Universidad Nacional de la Patagonia San Juan Bosco (Sede Trelew).

## Directorios y archivos de interes:
* El documento formal del práctico se encuentra [aquí](./TP3-MYS2020.pdf)
* Los gráficos se generan [aquí](./Graficos)
* Los ejercicios estan resueltos [aquí](./Ejercicios)
* El punto de entrada es [main.py](./main.py)

## Ejecución
Para la ejecución del presente proyecto basta con ejecutar en el directorio raíz:
> python main.py

***NOTA**: Tener en cuenta que dentro del archivo main.py se encuentra una variable (Por defecto en falso) que le indica al programa si debe imprimir en la consola los datos muestrales de los ejercicios que correspondan o no.*

# Resoluciones
A continuación se muestra la salida formateada de la ejecución del proyecto:

## Ejercicio 1
### Enunciado:
Investigue y documente al menos 3 lenguajes de programación que ofrezcan la posibilidad de generar números aleatorios que sigan una distribución probabilística ya sea de modo nativo o bien a través del uso de librerías externas. 
Genere un conjunto de 100 datos numéricos para una variable imaginaria y con ayuda de los lenguajes:
    a. Calcule la media.
    b. Calcule el desvío estándar.
    c. Calcule la varianza. 
    d. ¿Cómo se crean gráficos para interpretar los valores generados?
        
### Resolucion:
Para la resolucion de este ejercicio se eligieron los lenguajes Python, R e IDL
El desarrollo de python esta dado en el presente código, el de R se puede encontrar en este mismo repositorio (Con su correspondiente Dockerfile) y el de IDL se deja como investigacion unicamente
#### Inciso a: 
> La media nos da: 1.0498543807159086
#### Inciso b: 
> El desvío estándar nos da: 0.8718496863914285
#### Inciso c: 
> La variancia nos da: 0.7601218756608321
#### Inciso d: 
> Se utilizó la librería matplotlib en conjunto con la librerìa seaborn para la generación del gráfico (El mismo puede ser encontrado en El directorio de los ejercicios, identificado por el nombre del ejercicio).
> El gráfico correspondiente fue generado en el directorio de los Graficos, identificado por el ejercicio e inciso.

## Ejercicio 2
### Enunciado:
Teniendo en cuenta el material teórico y los lenguajes investigados en el punto 1, elija uno de ellos y genere 1000 valores de números aleatorios:
    a. Uniforme, con parámetros: Min: 0, Max: 1. 
    b. Normal, con parámetros: Media: 0, Desvío: 1.
    c. Poisson, con parámetro: λ = 5. 
    d. Exponencial, con parámetro:  = ¼. 
        
### Resolucion:
#### Inciso a (Uniforme):
> Media: 0.49066340523382823
> Desvío estándar: 0.2935888956369461
> Variancia: 0.08619443964132165
> El gráfico correspondiente fue generado en el directorio de los Graficos, identificado por el ejercicio e inciso.
#### Inciso b (Normal):
> Media: -0.02167358788348396
> Desvío estándar: 0.9927505380870746
> Variancia: 0.9855536308721761
> El gráfico correspondiente fue generado en el directorio de los Graficos, identificado por el ejercicio e inciso.
#### Inciso c (Poisson):
> Media: 5.06

> Desvío estándar: 2.18

> Variancia: 4.752400000000001

> El gráfico correspondiente fue generado en el directorio de los Graficos, identificado por el ejercicio e inciso.
#### Inciso d (Exponencial):
> Media: 3.867816835274043

> Desvio Estandar:4.114030552767647

> Varianza: 16.92524738910567

> El gráfico correspondiente fue generado en el directorio de los Graficos, identificado por el ejercicio e inciso.

## Ejercicio 3
### Enunciado:
A partir de la muestra obtenida de 1000 valores en el punto 2.a, realice:
    a. Una transformación para que la variable aleatoria tenga una distribución de probabilidad Exponencial con parámetro λ = 8.
    b. Un gráfico adecuado para variable aleatoria.
    c. Genere otros 100 valores aleatorios siguiendo una distribución Exponencial con parámetro λ = 2.
    d. Genere 1000 valores aleatorios con las mismas características que en el inciso c.
    e. Grafique los incisos c y d.
    f. Compare resultados para 100 y 1000 valores describiendo similitudes o diferencias encontradas.
        
### Resolucion:
#### Inciso a: Una transformación para que la variable aleatoria tenga una distribución de probabilidad Exponencial con parámetro λ = 8.
#### Inciso b: Un gráfico adecuado para variable aleatoria.
> El gráfico correspondiente fue generado en el directorio de los Graficos, identificado por el ejercicio e inciso.
#### Inciso c: Genere otros 100 valores aleatorios siguiendo una distribución Exponencial con parámetro λ = 2.
> Media: 0.4912709512247714

> Desvio Estandar:0.4309164414089023

> Variancia: 0.18568897947651192
#### Inciso d: Genere 1000 valores aleatorios con las mismas características que en el inciso c.
> Media: 4.260349305300635

> Desvio Estandar: 4.6939249179448375

> Variancia: 22.03293113530345
#### Inciso e: Grafique los incisos c y d.
> Los gráficos correspondientes fueron generados en el directorio de los Graficos, identificados por el ejercicio e inciso correspondientes.
#### Inciso f: Compare resultados para 100 y 1000 valores describiendo similitudes o diferencias encontradas.
> Contestado en el documento del trabajo

## Ejercicio 4
### Enunciado:
De acuerdo a los resultados obtenidos en el ejercicio anterior. Especifique:
    a. ¿Cuál fue la media muestral de los datos generados?
    b. ¿Cuál fue la varianza muestral de los datos generados?
    c. Realice un intervalo de confianza, con  3 desvíos estándar.
        
### Resolucion:
#### Inciso a:
> La media de los datos muestrales obtenidos en el inciso a fue de: 0.1292282228916112

> La media de los datos muestrales obtenidos en el inciso c fue de: 0.4912709512247714

> La media de los datos muestrales obtenidos en el inciso d fue de: 4.260349305300635
#### Inciso b:
> La variancia de los datos muestrales obtenidos en el inciso a fue de: 0.016316136809369646

> La variancia de los datos muestrales obtenidos en el inciso c fue de: 0.18568897947651192

> La variancia de los datos muestrales obtenidos en el inciso d fue de: 22.03293113530345
#### Inciso c:
> Los intervalos de confianza correspondientes al inciso a estan dados por: 
    Confianza: 0.1292282228916112(Izq: -3.9229834504721417 - Der: 4.181439896255364)

> Los intervalos de confianza correspondientes al inciso c estan dados por: 
    Confianza: 0.4912709512247714(Izq: -3.956054196448704 - Der: 4.9385960988982465)

> Los intervalos de confianza correspondientes al inciso d estan dados por: 
    Confianza: 4.260349305300635(Izq: -144.6481863085576 - Der: 153.16888491915884)
